package com.dtcloud.ots.utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * 功能：
 *  实现文件上传功能
 * @author：zhangyp(zhangyp@hncdcenter.com)
 * @create：2019-01-08 17:10:00
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
public class FileUtils {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     *  上传文件
     * @param file 文件
     * @param path 文件存放路径
     * @param fileName 源文件名
     * @return
     */
    public static boolean upload(MultipartFile file, String path, String fileName){
        //使用原文件名
        String realPath = path + "/" + fileName;
        File dest = new File(realPath);
        //判断文件父目录是否存在
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdir();
        }
        try {
            //保存文件
            file.transferTo(dest);
            return true;
        } catch (IllegalStateException e) {
            logger.error(e.getMessage());
            return false;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public static String singleFileUpload(MultipartFile file, String path){
        String result = "fail";
        InputStream ins;
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            final HttpPost postMethod = new HttpPost(path);
            ins = file.getInputStream();
            HttpEntity reqEntity = MultipartEntityBuilder.create().setCharset(Charset.forName("UTF-8")).addBinaryBody("file", ins, ContentType.DEFAULT_TEXT,file.getOriginalFilename()).build();
            postMethod.setEntity(reqEntity);
            CloseableHttpResponse res = client.execute(postMethod);
            result = EntityUtils.toString(res.getEntity(),"UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
