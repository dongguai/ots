package com.dtcloud.ots.utils.entity;


import java.io.Serializable;

/**
 * 功能：
 *     结果返回标准接口类
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-12 15:20:03
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
public class Result implements Serializable {
    private boolean flag; //返回结果状态
    private Integer code; //返回结果状态码
    private String message; //返回结果信息说明
    private Object data;  //请求返回结果具体内容
    private String url;   //请求的URL路径信息

    public Result(boolean flag, Integer code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    public Result(boolean flag, Integer code, String message, Object data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(boolean flag, Integer code, String message, String url) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.url = url;
    }

    public Result(boolean flag, Integer code, String message, Object data, String url) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
        this.url = url;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
