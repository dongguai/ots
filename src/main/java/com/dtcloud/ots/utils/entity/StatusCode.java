package com.dtcloud.ots.utils.entity;

/**
 * 功能：
 *      返回结果状态码,暂定后续可加
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-12 15:26:41
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
public interface StatusCode {
    int OK = 20000;//成功
    int ERROR = 20001;//失败
    int LOGINERROR = 20002;//用户名或密码错误
    int ACCESSERROR = 20003;//权限不足
    int REMOTEERROR = 20004;//远程调用失败
    int REPERROR = 20005;//重复操作
    //HTTP请求返回状态码 待定
    int HTTP_OK =200; //请求成功
    int HTTP_FOUND =302; //重定向
    int HTTP_BAD_REQUEST =400;  //请求语法存在错误
    int HTTP_NOT_FOUND = 404;  //服务器未找到请求的资源误
    int HTTP_SERVER_ERROR =500;  //服务器本身发生错误
}
