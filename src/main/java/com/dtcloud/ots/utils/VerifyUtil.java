package com.dtcloud.ots.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: VerifyUtil
 * 功 能: 校验工具类,待完善
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-23 10:55:58
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
public class VerifyUtil {

    /**
     * 检查手机号是否合法
     *
     * @param mobile
     * @return
     */
    public static boolean verifyPhone(String mobile) {
        String regExp =  "^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(mobile);
        return m.matches();
    }

    /**
     * 检查密码是否合法
     *   8-16位数字、字母或其组合的密码
     * @param password
     * @return
     */
    public static boolean verifyPwd(String password){
        String regExp =  "^[a-zA-Z0-9]{8,16}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(password);
        return m.matches();
    }

    /**
     * 检查昵称是否合法
     *
     * @param nick_name
     * @return
     */
    public static boolean verifyNickName(String nick_name){
        if(nick_name.length()>0&&nick_name.length()<=12){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 检查邮箱是否合法
     * @param email
     * @return
     */
    public static boolean verifyEmail(String email){
        String regExp =  "^(www\\.)?\\w+@\\w+(\\.\\w+)+$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * 检验微信是否合法
     * @param weixin
     * @return
     */
    public static boolean verifyWeixin(String weixin){
        String regExp =  "^[a-zA-Z]{1}[-_a-zA-Z0-9]{5,19}+$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(weixin);
        return m.matches();
    }

    /**
     * 检验qq是否合法
     * @param qq
     * @return
     */
    public static boolean verifyQq(String qq){
        String regExp =  "[1-9][0-9]{4,12}";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(qq);
        return m.matches();
    }

}
