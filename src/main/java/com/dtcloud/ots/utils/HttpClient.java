package com.dtcloud.ots.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: HttpClient
 * 功 能:  httpclient客户端处理工具类  处理json/xml/map请求
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-21 22:06:12
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
public class HttpClient {

    private static final Logger log = LoggerFactory.getLogger(HttpClient.class);


    public static String postJSON(String url, JSONObject jsonObject, String encoding) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        String response = null;
        try {
            StringEntity entityParams = new StringEntity(jsonObject.toString());
            entityParams.setContentEncoding(encoding);
            entityParams.setContentType("application/json");//发送json数据需要设置contentType
            httpPost.setEntity(entityParams);
            HttpResponse resp = httpclient.execute(httpPost);
            if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(resp.getEntity());// 返回json格式：
                response = JSONObject.toJSONString(result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }


    public static String postXML(String url, String xmlFileName) {
        log.info(url+":"+xmlFileName);
        CloseableHttpClient client = null;
        CloseableHttpResponse resp = null;
        String response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "text/xml; charset=UTF-8");
            client = HttpClients.createDefault();
            StringEntity entityParams = new StringEntity(xmlFileName, "utf-8");
            httpPost.setEntity(entityParams);
            client = HttpClients.createDefault();
            resp = client.execute(httpPost);
            String result = EntityUtils.toString(resp.getEntity(), "utf-8");
            response = JSONObject.toJSONString(result);
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            try {
                if (client != null) {
                    client.close();
                }
                if (resp != null) {
                    resp.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;

    }

    public static String postMAP(String url, Map<String, String> map, String charset) {
        log.info(url + ":" + map);
        String response = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //配置超时时间
        RequestConfig requestConfig = RequestConfig.custom().
                setConnectTimeout(10000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(10000).setRedirectsEnabled(true).build();

        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("content-type", "application/x-www-form-urlencoded");
        //设置超时时间
        httpPost.setConfig(requestConfig);
        //装配post请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        for (Map.Entry<String, String> maps : map.entrySet()) {
            list.add(new BasicNameValuePair(maps.getKey(), maps.getValue()));
        }
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, "UTF-8");
            //设置post求情参数
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            String strResult = "";
            if (httpResponse != null) {
                System.out.println(httpResponse.getStatusLine().getStatusCode());
                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                    strResult = EntityUtils.toString(httpResponse.getEntity());
                } else if (httpResponse.getStatusLine().getStatusCode() == 400) {
                    strResult = EntityUtils.toString(httpResponse.getEntity());
                } else if (httpResponse.getStatusLine().getStatusCode() == 500) {
                    strResult = "Error Response: " + httpResponse.getStatusLine().toString();
                } else {
                    strResult = "Error Response: " + httpResponse.getStatusLine().toString();
                }
            }
            response = JSONObject.toJSONString(strResult);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (httpClient != null) {
                    httpClient.close(); //释放资源
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

}
