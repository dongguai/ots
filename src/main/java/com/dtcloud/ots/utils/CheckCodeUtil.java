package com.dtcloud.ots.utils;

import java.util.Random;

/**
 * 功能：
 *      校验码生成工具(提供默认的6位数字验证码)
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-14 18:43:57
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
public class CheckCodeUtil {

    /**
     * 生成随机的验证码字符串
     * @param length 字符串长度
     * @param type (0: 仅数字; 2:仅字符; 别的数字:数字和字符)
     * @return
     */
    public static String getRandomStr(int length, int type){
        String str = "";
        int beginChar = 'a';
        int endChar = 'z';

        // 只有数字
        if (type == 0){
            beginChar = 'z' + 1;
            endChar = 'z' + 10;
        }
        // 只有小写字母
        else if (type == 2){
            beginChar = 'a';
            endChar = 'z';
        }
        // 有数字和字母
        else{
            beginChar = 'a';
            endChar = 'z' + 10;
        }

        // 生成随机类
        Random random = new Random();
        for (int i = 0; i < length; i++){
            int tmp = (beginChar + random.nextInt(endChar - beginChar));
            // 大于'z'的是数字
            if (tmp > 'z'){
                tmp = '0' + (tmp - 'z');
            }
            str += (char) tmp;
        }

        return str;
    }

    /**
     * 提供默认方法获取6位的通用数字验证码
     * @return
     */
    public static String getCheckNum() {
        return getRandomStr(6, 0);
    }
}
