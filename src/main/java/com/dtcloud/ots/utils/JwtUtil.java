package com.dtcloud.ots.utils;

import com.dtcloud.ots.manage.model.sys.SysResource;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;
import java.util.List;

/**
 * 功能：
 *      jwt工具类 生成token和解析token
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-12 15:15:37
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
@ConfigurationProperties("jwt.config")
public class JwtUtil {
    //指定token的头信息
    public static final String TOKEN_HEADER="Authorization";
    //指定token前端带过来的开头
    public static final String TOKEN_PREFIX = "Bearer ";
    //该jwt的签发者
    private static final String ISS = "DtCloud";
    //token的秘钥,存于服务器
    private String key ;
    //过期时间在配置文件配置
    private long ttl ;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        this.ttl = ttl;
    }

    /**
     * 生成JWT
     *
     * @param id
     * @param username
     * @return
     */
    public String createJWT(String id, String username, String roles, List<SysResource> resourceList) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        JwtBuilder builder = Jwts.builder()
                .setIssuer(ISS)
                .setId(id)
                .setSubject(username)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, key)
                .claim("roles", roles)  //用户对应的角色
                .claim("resource", resourceList);  //对应角色的所拥有的资源列表
        if (ttl > 0) {
            builder.setExpiration( new Date( nowMillis + ttl));
        }
        return builder.compact();
    }

    /**
     * 解析JWT
     * @param token
     * @return
     */
    public Claims parseJWT(String token){
        return  Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * 验证token是否已过期
     * @param token
     * @return
     */
    public boolean isExpiration(String token){
        return parseJWT(token).getExpiration().before(new Date());
    }

}