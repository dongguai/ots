package com.dtcloud.ots;

import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.JwtUtil;
import com.dtcloud.ots.utils.SmsUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class OtsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtsApplication.class, args);
    }
    /**
     * jwt工具类
     * @return
     */
    @Bean
    public JwtUtil jwtUtil(){
        return new JwtUtil();
    }

    /**
     * Spring security 密码加密类
     * @return
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 阿里大于短信发送工具类
     * @return
     */
    @Bean
    public SmsUtil smsUtil(){
        return new SmsUtil();
    }

    /**
     * id生成器
     * @return
     */
    @Bean
    public IdWorker idWorker(){
        return new IdWorker(0,0);
    }

}
