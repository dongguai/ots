package com.dtcloud.ots.manage.dao.sys;


import com.dtcloud.ots.manage.model.sys.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SysRoleDao extends JpaRepository<SysRole,Integer>,JpaSpecificationExecutor<SysRole> {
	
}
