package com.dtcloud.ots.manage.dao.gen;

import com.dtcloud.ots.manage.model.gen.ProvinceCityDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ProvinceCityDistrictDao extends JpaRepository<ProvinceCityDistrict,String>,JpaSpecificationExecutor<ProvinceCityDistrict>{
	
}
