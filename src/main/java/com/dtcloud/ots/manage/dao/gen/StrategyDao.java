package com.dtcloud.ots.manage.dao.gen;


import com.dtcloud.ots.manage.model.gen.Strategy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface StrategyDao extends JpaRepository<Strategy,String>,JpaSpecificationExecutor<Strategy> {
    //根据id和登录用户的id查找对应的攻略
    Strategy findByIdAndUserID(String id, int userID);
    //根据id和登录用户的id删除对应的攻略
    @Modifying
    void deleteByIdAndUserID(String id, int userID);

    //查询目标表中的sort字段的最大值
    @Query(value = "select max(sort) FROM Strategy",nativeQuery = true)
    Integer findMaxSort();

}
