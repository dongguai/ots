package com.dtcloud.ots.manage.dao.gen;


import com.dtcloud.ots.manage.model.gen.Headline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface HeadlineDao extends JpaRepository<Headline,String>,JpaSpecificationExecutor<Headline> {
    /**
     * 根据token中的用户id和头条的id查找头条信息
     * @param id
     * @param userID
     * @return
     */
    Headline findByIdAndUserID(String id,Integer userID);

    /**
     * 根据登录的用户id 查找对应的头条集合
     * @param userID
     * @return
     */
    List<Headline> findByUserIDOrderByCreated(int userID);

    /**
     * 根据id和登录用户的id删除对应的头条
     * @param id
     * @param userID
     */
    void deleteByIdAndUserID(String id, int userID);
}
