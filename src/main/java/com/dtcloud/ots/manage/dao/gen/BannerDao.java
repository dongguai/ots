package com.dtcloud.ots.manage.dao.gen;


import com.dtcloud.ots.manage.model.gen.Banner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface BannerDao extends JpaRepository<Banner,String>,JpaSpecificationExecutor<Banner> {
    @Modifying
    void deleteByIdAndUserID(String id, int userID);

    Banner findByIdAndUserID(String id, int userID);

    //查询目标表中的sort字段的最大值
    @Query(value = "select max(sort) FROM banner",nativeQuery = true)
    Integer findMaxSort();
}
