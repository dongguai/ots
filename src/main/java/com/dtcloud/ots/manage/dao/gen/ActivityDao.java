package com.dtcloud.ots.manage.dao.gen;


import com.dtcloud.ots.manage.model.gen.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ActivityDao extends JpaRepository<Activity,String>,JpaSpecificationExecutor<Activity> {
    //根据登录用户的查找自己创建的活动信息.
    List<Activity> findByUserID(Integer userID);

    //根据资讯的id和登录用户的id查找对应的活动信息
    Activity findByIdAndUserID(String id, int userID);

    //根据id和登录用户的id删除对应的活动
    @Modifying
    void deleteByIdAndUserID(String id, int userID);
}
