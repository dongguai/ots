package com.dtcloud.ots.manage.dao.sys;


import com.dtcloud.ots.manage.model.sys.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SysUserDao extends JpaRepository<SysUser,Integer>,JpaSpecificationExecutor<SysUser> {

    //根据用户名查找用户
    SysUser findByLoginname(String loginName);
}
