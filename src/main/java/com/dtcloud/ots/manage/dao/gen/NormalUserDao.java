package com.dtcloud.ots.manage.dao.gen;

import com.dtcloud.ots.manage.model.gen.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface NormalUserDao extends JpaRepository<User,String>,JpaSpecificationExecutor<User>{
    /**
     * 根据用户名(手机号查找用户信息)
     * @param mobile
     * @return
     */
    User findByMobile(String mobile);
	
}
