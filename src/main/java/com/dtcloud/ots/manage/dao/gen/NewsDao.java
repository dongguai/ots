package com.dtcloud.ots.manage.dao.gen;

import com.dtcloud.ots.manage.model.gen.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface NewsDao extends JpaRepository<News,String>,JpaSpecificationExecutor<News> {

    /**
     * 根据用户登录的id查找全部的资讯信息 并按照时间倒叙
     * @param userID
     * @return
     */
    List<News> findByUserIDOrderByCreatedDesc(int userID);

    /**
     * 根据资讯的id和登录用户的id查找对应的资讯信息
     * @param id
     * @param userID
     * @return
     */
    News findByIdAndUserID(String id, int userID);

    /**
     * 根据id和登录用户的id删除对应的资讯
     * @param id
     * @param userID
     */
    @Modifying
    void deleteByIdAndUserID(String id, int userID);
}
