package com.dtcloud.ots.manage.interceptor;


import com.dtcloud.ots.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能：
 *      jwt拦截类定义
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-14 21:26:05
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
@Component
public class JwtInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtUtil jwtUtil;

    /**
     * 拦截器在所有请求进来之前进行处理拦截并处理token信息
     * @param request
     * @param response
     * @param handler
     * @return
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //获取请求头里面的鉴权信息
        String authorization = request.getHeader("Authorization");
        //鉴权信息和前台约定为:Bearer+" "+token的格式
        if (authorization != null && authorization.startsWith("Bearer ")) {
            String token = authorization.substring(7);
            Claims claims = jwtUtil.parseJWT(token);
            if (claims != null) {
/*                if ("user".equals(claims.get("roles"))){
                    request.setAttribute("user_claims", claims);
                }*/
                request.setAttribute("userID", claims.getId());
            }
        }
        return true;
    }
}
