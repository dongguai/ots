package com.dtcloud.ots.manage.service.common;


import com.dtcloud.ots.manage.dao.gen.NormalUserDao;
import com.dtcloud.ots.manage.model.gen.User;
import com.dtcloud.ots.utils.CheckCodeUtil;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.JwtUtil;
import com.dtcloud.ots.utils.SmsUtil;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 功能：
 * 普通用户功能实现
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-18 14:50:41
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
@Service
public class UserService {

    @Autowired
    private NormalUserDao userDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private BCryptPasswordEncoder bc;

    @Autowired
    private SmsUtil smsUtil;

    @Autowired
    private JwtUtil jwtUtil;

    @Value("${aliyun.sms.template_code}")
    private String template_code;
    @Value("${aliyun.sms.sign_name}")
    private String sign_name;

    /**
     * 发送验证码
     *
     * @param mobile
     */
    public void sendsms(String mobile) {
        try {
            //生成6位的验证码
            String smscode = CheckCodeUtil.getCheckNum();
            //保存该验证码到redis中.并设置过期时间为5分钟
            redisTemplate.boundValueOps("smscode_" + mobile).set(smscode + "", 5, TimeUnit.MINUTES);
            smsUtil.sendSms(mobile, template_code, sign_name, "{\"code\":\"" + smscode + "\"}");
        } catch (Exception e) {
            throw new RuntimeException("发送短信验证码失败,请重试");
        }
    }

    /**
     * 用户注册的时候校验验证逻辑
     * @param map
     * @return
     */
    public Result registerCheckCode(Map<String, String> map) {
        String mobile = map.get("mobile");
        String code = map.get("code");
        User user=findByMobile(mobile);
        if (user!=null){
            return new Result(false, StatusCode.ERROR, "该账号已注册");
        }
        if (checkCode(mobile, code)){
            return new Result(true, StatusCode.OK, "验证通过");
        }
        return new Result(false, StatusCode.ERROR, "验证码错误,请重试");

    }

    /**
     * 用户注册逻辑实现
     *
     * @param map
     */
    public Result register(Map<String, String> map) {
        try {
            String mobile = map.get("mobile");
            String password = map.get("password");
            String code = map.get("code");
            //再次校验验证码,防止跳过验证码阶段直接注册
            if (code == null || !checkCode(mobile, code)) {
                return new Result(false, StatusCode.ERROR, "注册超时");
            }
            User user = new User();
            user.setId(idWorker.nextId() + "");
            user.setMobile(mobile);
            //密码加密
            user.setPassword(bc.encode(password));
            user.setRegister_time(new Date());
            user.setModifytime(new Date());
            //刚注册用户状态标记为正常
            user.setState("1");
            userDao.save(user);
            return new Result(true, StatusCode.OK, "注册成功");
        } catch (Exception e) {
            return new Result(false, StatusCode.ERROR, "注册失败,请重试");
        }
    }

    /**
     * 用户登录逻辑实现
     *
     * @param map
     * @return
     */
    public Result login(Map<String, String> map) {
        String mobile = map.get("mobile");
        String password = map.get("password");
        String code = map.get("code");
        User user = findByMobile(mobile);
        if (user==null){
            return new Result(false, StatusCode.ACCESSERROR, "当前账号不存在");
        }
        if (code != null && !"".equals(code)) {
            if (!checkCode(mobile, code)) {
                return new Result(false, StatusCode.ERROR, "验证码错误,请重试");
            }
        } else {
            user = findByMobile(mobile);
            if (!bc.matches(password, user.getPassword())) {
                user = null;
            }
        }
        if (user != null) {
            //登录成功后生成token
            String token = jwtUtil.createJWT(user.getId(), user.getNick_name(), "user", null);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("token", token);
            resultMap.put("userId", user.getId());
            resultMap.put("mobile", user.getMobile());
            resultMap.put("nickname", user.getNick_name());
            resultMap.put("icon", user.getIcon());
            return new Result(true, StatusCode.OK, "登录成功", resultMap);
        }
        return new Result(false, StatusCode.LOGINERROR, "用户名或密码错误");
    }

    /**
     * 校验校验码是否正确
     *
     * @param mobile
     * @param code
     * @return
     */
    public boolean checkCode(String mobile, String code) {
        String smscode = (String) redisTemplate.boundValueOps("smscode_" + mobile).get();
        if (smscode == null || "".equals(smscode)) {
            return false;
        }
        if (smscode.equals(code)) {
            return true;
        }
        return false;
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public User findById(String id) {
        return userDao.findById(id).get();
    }


    /**
     * 根据用户手机号查找用户信息
     *
     * @param mobile
     * @return
     */
    private User findByMobile(String mobile) {
        return userDao.findByMobile(mobile);
    }

    /**
     * 修改
     *
     * @param user
     */
    public void update(User user) {
        userDao.save(user);
    }



}
