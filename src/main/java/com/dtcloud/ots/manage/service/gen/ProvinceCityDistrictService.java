package com.dtcloud.ots.manage.service.gen;


import com.dtcloud.ots.manage.dao.gen.ProvinceCityDistrictDao;
import com.dtcloud.ots.manage.model.gen.ProvinceCityDistrict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ProvinceCityDistrictService {

	@Autowired
	private ProvinceCityDistrictDao provinceCityDistrictDao;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<ProvinceCityDistrict> findAll() {
		return provinceCityDistrictDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<ProvinceCityDistrict> findSearch(Map whereMap, int page, int size) {
		Specification<ProvinceCityDistrict> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return provinceCityDistrictDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<ProvinceCityDistrict> findSearch(Map whereMap) {
		Specification<ProvinceCityDistrict> specification = createSpecification(whereMap);
		return provinceCityDistrictDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public ProvinceCityDistrict findById(String id) {
		return provinceCityDistrictDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param cityDistrict
	 */
	public void add(ProvinceCityDistrict cityDistrict) {
		provinceCityDistrictDao.save(cityDistrict);
	}

	/**
	 * 修改
	 * @param cityDistrict
	 */
	public void update(ProvinceCityDistrict cityDistrict) {
		provinceCityDistrictDao.save(cityDistrict);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		provinceCityDistrictDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<ProvinceCityDistrict> createSpecification(Map searchMap) {

		return new Specification<ProvinceCityDistrict>() {

			@Override
			public Predicate toPredicate(Root<ProvinceCityDistrict> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 地区id
                if (searchMap.get("pcd_id")!=null && !"".equals(searchMap.get("pcd_id"))) {
                	predicateList.add(cb.like(root.get("pcd_id").as(String.class), "%"+(String)searchMap.get("pcd_id")+"%"));
                }
                // 地区名称
                if (searchMap.get("pcd_name")!=null && !"".equals(searchMap.get("pcd_name"))) {
                	predicateList.add(cb.like(root.get("pcd_name").as(String.class), "%"+(String)searchMap.get("pcd_name")+"%"));
                }
                // 地区父id
                if (searchMap.get("parent_id")!=null && !"".equals(searchMap.get("parent_id"))) {
                	predicateList.add(cb.like(root.get("parent_id").as(String.class), "%"+(String)searchMap.get("parent_id")+"%"));
                }
                // 地区号
                if (searchMap.get("area_code")!=null && !"".equals(searchMap.get("area_code"))) {
                	predicateList.add(cb.like(root.get("area_code").as(String.class), "%"+(String)searchMap.get("area_code")+"%"));
                }
                // 地区层级
                if (searchMap.get("pcd_level")!=null && !"".equals(searchMap.get("pcd_level"))) {
                	predicateList.add(cb.like(root.get("pcd_level").as(String.class), "%"+(String)searchMap.get("pcd_level")+"%"));
                }
                // 地区类型
                if (searchMap.get("type")!=null && !"".equals(searchMap.get("type"))) {
                	predicateList.add(cb.like(root.get("type").as(String.class), "%"+(String)searchMap.get("type")+"%"));
                }
                // 地区汉语拼音
                if (searchMap.get("pin_yin")!=null && !"".equals(searchMap.get("pin_yin"))) {
                	predicateList.add(cb.like(root.get("pin_yin").as(String.class), "%"+(String)searchMap.get("pin_yin")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
