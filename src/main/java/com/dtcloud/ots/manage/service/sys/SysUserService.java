package com.dtcloud.ots.manage.service.sys;

import com.dtcloud.ots.manage.dao.sys.SysUserDao;
import com.dtcloud.ots.manage.model.gen.User;
import com.dtcloud.ots.manage.model.sys.SysUser;
import com.dtcloud.ots.utils.JwtUtil;
import com.dtcloud.ots.utils.VerifyUtil;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

/**
 * 系统用户服务层实现
 *
 * @author Administrator
 */
@Service
public class SysUserService {

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private BCryptPasswordEncoder bc;

    @Autowired
    private JwtUtil jwtUtil;

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<SysUser> findAll() {
        return sysUserDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<SysUser> findSearch(Map whereMap, int page, int size) {
        Specification<SysUser> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return sysUserDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<SysUser> findSearch(Map whereMap) {
        Specification<SysUser> specification = createSpecification(whereMap);
        return sysUserDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public SysUser findById(Integer id) {
        return sysUserDao.findById(id).get();
    }

    /**
     * 增加或修改用户
     *
     * @param user
     */
    public Result add(SysUser user) {
        SysUser sysUser = sysUserDao.findByLoginname(user.getLoginname());
        //用户存在时候属于修改用户信息
        if (sysUser != null) {
            if (user.getLoginpassword() != null) {
                if (!VerifyUtil.verifyPwd(user.getLoginpassword())) {
                    throw new RuntimeException("请填写8-16位数字、字母或其组合的密码");
                }else {
                    user.setLoginpassword(bc.encode(user.getLoginpassword()));
                }
            }
            user.setId(sysUser.getId());
            user.setLoginpassword(sysUser.getLoginpassword());
            user.setCreated(sysUser.getCreated());
            user.setUpdated(new Date());
            sysUserDao.save(user);
            return new Result(true, StatusCode.OK, "用户信息修改成功");
        } else {
            //增加新用户
            if (!VerifyUtil.verifyPwd(user.getLoginpassword())) {
                throw new RuntimeException("请填写8-16位数字、字母或其组合的密码");
            }
            //加密密码
            user.setLoginpassword(bc.encode(user.getLoginpassword()));
            user.setCreated(new Date());
            user.setUpdated(new Date());
            sysUserDao.save(user);
            return new Result(true, StatusCode.OK, "增加用户成功");
        }

    }

    /**
     * 修改
     *
     * @param user
     */
    public void update(SysUser user) {
        sysUserDao.save(user);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(Integer id) {
        sysUserDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<SysUser> createSpecification(Map searchMap) {

        return new Specification<SysUser>() {

            @Override
            public Predicate toPredicate(Root<SysUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // 系统用户名,5-16位数字、字母或其组合的账号
                if (searchMap.get("loginname") != null && !"".equals(searchMap.get("loginname"))) {
                    predicateList.add(cb.like(root.get("loginname").as(String.class), "%" + (String) searchMap.get("loginname") + "%"));
                }
                // 登录密码
                if (searchMap.get("loginpassword") != null && !"".equals(searchMap.get("loginpassword"))) {
                    predicateList.add(cb.like(root.get("loginpassword").as(String.class), "%" + (String) searchMap.get("loginpassword") + "%"));
                }
                // 真实名称
                if (searchMap.get("realname") != null && !"".equals(searchMap.get("realname"))) {
                    predicateList.add(cb.like(root.get("realname").as(String.class), "%" + (String) searchMap.get("realname") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

    /**
     * 根据用户登录名和密码查找对应用户
     *
     * @param loginName
     * @param password
     * @return
     */
    public SysUser findByLoginName(String loginName, String password) {
        SysUser sysUser = sysUserDao.findByLoginname(loginName);
        if (sysUser == null) {
            throw new RuntimeException("当前用户不存在"); //异常未细分,后期再统一处理异常
        }
        if (sysUser != null && bc.matches(password, sysUser.getLoginpassword())) {
            return sysUser;
        }
        return null;
    }

    /**
     * 系统用户登录验证
     *
     * @param map
     * @return
     */
    public Result login(Map<String, String> map) {
        String loginname = map.get("loginname");
        String loginpassword = map.get("loginpassword");
        SysUser sysUser = findByLoginName(loginname, loginpassword);
        if (sysUser != null) {
            //登录成功后生成token  暂时设定为admin
            //查找该用户对应的角色和权限并放入到token中
            String token = jwtUtil.createJWT(sysUser.getId() + "", sysUser.getRealname(), "admin", null);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("token", token);
            resultMap.put("userId", sysUser.getId());
            resultMap.put("nickname", sysUser.getNick_name());
            resultMap.put("icon", sysUser.getIcon());
            return new Result(true, StatusCode.OK, "登录成功", resultMap);
        }
        return new Result(false, StatusCode.LOGINERROR, "用户名或密码错误");
    }
}
