package com.dtcloud.ots.manage.service.gen;

import com.dtcloud.ots.manage.dao.gen.StrategyDao;
import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.manage.model.gen.News;
import com.dtcloud.ots.manage.model.gen.Strategy;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class StrategyService {

	@Autowired
	private StrategyDao strategyDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Strategy> findAll() {
		return strategyDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Strategy> findSearch(Map whereMap, int page, int size) {
		Specification<Strategy> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return strategyDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Strategy> findSearch(Map whereMap) {
		Specification<Strategy> specification = createSpecification(whereMap);
		return strategyDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Strategy findById(String id) {
		return strategyDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param strategy
	 */
	public void add(Strategy strategy) {
		strategy.setId( idWorker.nextId()+"" );
		//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
		strategy.setState("0");
		strategy.setUpdated(new Date());
		strategyDao.save(strategy);
	}

	/**
	 * 修改
	 * @param strategy
	 */
	public void update(Strategy strategy) {
		strategy.setUpdated(new Date());
		strategy.setState("0");
		strategyDao.save(strategy);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		strategyDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Strategy> createSpecification(Map searchMap) {

		return new Specification<Strategy>() {

			@Override
			public Predicate toPredicate(Root<Strategy> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 攻略id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 攻略标题
                if (searchMap.get("title")!=null && !"".equals(searchMap.get("title"))) {
                	predicateList.add(cb.like(root.get("title").as(String.class), "%"+(String)searchMap.get("title")+"%"));
                }
                // 攻略缩略图链接
                if (searchMap.get("image")!=null && !"".equals(searchMap.get("image"))) {
                	predicateList.add(cb.like(root.get("image").as(String.class), "%"+(String)searchMap.get("image")+"%"));
                }
                // 是否顶置(1是,0不是,默认0)
                if (searchMap.get("istop")!=null && !"".equals(searchMap.get("istop"))) {
                	predicateList.add(cb.like(root.get("istop").as(String.class), "%"+(String)searchMap.get("istop")+"%"));
                }
                // 攻略作者姓名
                if (searchMap.get("author")!=null && !"".equals(searchMap.get("author"))) {
                	predicateList.add(cb.like(root.get("author").as(String.class), "%"+(String)searchMap.get("author")+"%"));
                }
                // 攻略内容
                if (searchMap.get("strategycontent")!=null && !"".equals(searchMap.get("strategycontent"))) {
                	predicateList.add(cb.like(root.get("strategycontent").as(String.class), "%"+(String)searchMap.get("strategycontent")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

	/**
	 * 更新状态,用于审核是否发布
	 * @param map
	 * @return
	 */
	public Result updateStatus(Map<String, String> map) {
		String id = map.get("id");
		String state = map.get("state");
		String userID = map.get("userID");
		try {
			Strategy strategy = strategyDao.findById(id).get();
			if (strategy.getUserID()!=Integer.parseInt(userID)){
				throw new RuntimeException("审批异常");
			}
			strategy.setState(state);
			strategy.setUpdated(new Date());
			strategyDao.save(strategy);
			return new Result(true, StatusCode.OK, "操作成功");
		} catch(Exception e) {
			throw new RuntimeException("审批异常,请重试");
		}
	}

	/**
	 * 根据id和登录用户的id查找对应的 攻略
	 * @param id
	 * @param userID
	 * @return
	 */
	public Result findByIdandUserID(String id, int userID) {
		try {
			Strategy strategy = strategyDao.findByIdAndUserID(id, userID);
			return new Result(true, StatusCode.OK, "查询成功", strategy,null);
		} catch (Exception e) {
			throw new RuntimeException("查询异常,请重试");
		}
	}

	/**
	 * 根据id和登录用户的id删除对应的 攻略
	 * @param id
	 * @param userID
	 * @return
	 */
	@Transactional
	public Result deleteByIdandUserID(String id, int userID) {
		try {
		    strategyDao.deleteByIdAndUserID(id,userID);
		    return new Result(true, StatusCode.OK, "删除成功");
		} catch(Exception e) {
			throw new RuntimeException("删除异常,请重试");
		}
	}
}
