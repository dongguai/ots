package com.dtcloud.ots.manage.service.gen;


import com.dtcloud.ots.manage.dao.gen.BannerDao;
import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.manage.model.gen.Banner;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class BannerService {

	@Autowired
	private BannerDao bannerDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Banner> findAll() {
		return bannerDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Banner> findSearch(Map whereMap, int page, int size) {
		Specification<Banner> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return bannerDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Banner> findSearch(Map whereMap) {
		Specification<Banner> specification = createSpecification(whereMap);
		return bannerDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Banner findById(String id) {
		return bannerDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param banner
	 */
	public void add(Banner banner) {
		banner.setId( idWorker.nextId()+"" );
		banner.setCreated(new Date());
		banner.setUpdated(new Date());
		//表格默认按照位置序号正序排列，新添加的默认位置为1
        Integer maxSort = bannerDao.findMaxSort();
        if (maxSort==null){
            banner.setSort(1);
        }else {
            banner.setSort(maxSort+1);
        }
		bannerDao.save(banner);
	}

	/**
	 * 修改
	 * @param banner
	 */
	public void update(Banner banner) {
		banner.setUpdated(new Date());
		//修改后,需要更新状态为未发布状态
		banner.setState("0");
		bannerDao.save(banner);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		bannerDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Banner> createSpecification(Map searchMap) {

		return new Specification<Banner>() {

			@Override
			public Predicate toPredicate(Root<Banner> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 导航id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 导航标题
                if (searchMap.get("title")!=null && !"".equals(searchMap.get("title"))) {
                	predicateList.add(cb.like(root.get("title").as(String.class), "%"+(String)searchMap.get("title")+"%"));
                }
                // 导航图片路径
                if (searchMap.get("image")!=null && !"".equals(searchMap.get("image"))) {
                	predicateList.add(cb.like(root.get("image").as(String.class), "%"+(String)searchMap.get("image")+"%"));
                }
                // 首页banner链接类型，其中1外链，2头条，3攻略，4咨询，5活动
                if (searchMap.get("link")!=null && !"".equals(searchMap.get("link"))) {
                	predicateList.add(cb.like(root.get("link").as(String.class), "%"+(String)searchMap.get("link")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}

	/**
	 * 更新状态,用于审核是否发布
	 * @param map
	 * @return
	 */
	public Result updateStatus(Map<String, String> map) {
		String id = map.get("id");
		String state = map.get("state");
		String userID = map.get("userID");
		try {
			Banner banner = bannerDao.findById(id).get();
			if (banner.getUserID()!=Integer.parseInt(userID)){
				throw new RuntimeException("审批异常,请重新登录后操作");
			}
			banner.setState(state);
			banner.setUpdated(new Date());
			bannerDao.save(banner);
			return new Result(true, StatusCode.OK, "操作成功");
		} catch (RuntimeException e) {
			throw new RuntimeException("审批异常,请重新登录后操作");
		}
	}

	/**
	 * 根据id和登录用户的id删除 banner 业务实现
	 * @param id
	 * @param userID
	 * @return
	 */
	@Transactional
	public Result deleteByIdandUserID(String id, int userID) {
		try {
			bannerDao.deleteByIdAndUserID(id,userID);
			return new Result(true, StatusCode.OK, "删除成功");
		} catch(Exception e) {
			throw new RuntimeException("删除异常,请重试");
		}
	}

	/**
	 * 根据id和登录用户的id  查找banner
	 * @param id
	 * @param userID
	 * @return
	 */
	public Result findByIdandUserID(String id, int userID) {
		try {
			Banner banner = bannerDao.findByIdAndUserID(id, userID);
			return new Result(true, StatusCode.OK, "查询成功", banner,null);
		} catch (Exception e) {
			throw new RuntimeException("查询异常,请重试");
		}
	}
}
