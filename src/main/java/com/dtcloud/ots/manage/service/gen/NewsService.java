package com.dtcloud.ots.manage.service.gen;

import com.dtcloud.ots.manage.dao.gen.NewsDao;
import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.manage.model.gen.Banner;
import com.dtcloud.ots.manage.model.gen.News;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class NewsService {

	@Autowired
	private NewsDao newsDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<News> findAll() {
		return newsDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<News> findSearch(Map whereMap, int page, int size) {
		Specification<News> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return newsDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<News> findSearch(Map whereMap) {
		Specification<News> specification = createSpecification(whereMap);
		return newsDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public News findById(String id) {
		return newsDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param news
	 */
	public void add(News news) {
		news.setId( idWorker.nextId()+"" );
		news.setUpdated(new Date());
		news.setState("0");
		newsDao.save(news);
	}

	/**
	 * 修改
	 * @param news
	 */
	public void update(News news) {
		news.setUpdated(new Date());
		//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0  改后需要重置为未发布状态
		news.setState("0");
		newsDao.save(news);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		newsDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<News> createSpecification(Map searchMap) {

		return new Specification<News>() {

			@Override
			public Predicate toPredicate(Root<News> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 资讯id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 资讯标题
                if (searchMap.get("title")!=null && !"".equals(searchMap.get("title"))) {
                	predicateList.add(cb.like(root.get("title").as(String.class), "%"+(String)searchMap.get("title")+"%"));
                }
                // 资讯图片地址
                if (searchMap.get("image")!=null && !"".equals(searchMap.get("image"))) {
                	predicateList.add(cb.like(root.get("image").as(String.class), "%"+(String)searchMap.get("image")+"%"));
                }
                // 资讯内容
                if (searchMap.get("content")!=null && !"".equals(searchMap.get("content"))) {
                	predicateList.add(cb.like(root.get("content").as(String.class), "%"+(String)searchMap.get("content")+"%"));
                }
                // 标签状态, 0热门,1顶置,2热门顶置全选
                if (searchMap.get("label")!=null && !"".equals(searchMap.get("label"))) {
                	predicateList.add(cb.like(root.get("label").as(String.class), "%"+(String)searchMap.get("label")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

	/**
	 * 更新状态,用于审核是否发布
	 * @param map
	 * @return
	 */
	public Result updateStatus(Map<String, String> map) {
		String id = map.get("id");
		String state = map.get("state");
		String userID = map.get("userID");
		try {
			News news = newsDao.findById(id).get();
			if (news.getUserID()!=Integer.parseInt(userID)){
				throw new RuntimeException("审批异常");
			}
			news.setState(state);
			news.setUpdated(new Date());
			newsDao.save(news);
			return new Result(true, StatusCode.OK, "操作成功");
		} catch(Exception e) {
			throw new RuntimeException("审批异常,请重试");
		}

	}

	/**
	 * @param userID
	 * @return
	 */
	public Result findByUserID(int userID) {
		List<News> newsList = newsDao.findByUserIDOrderByCreatedDesc(userID);
		if (newsList==null){
			return new Result(true, StatusCode.OK, "无资讯信息");
		}
		return new Result(true, StatusCode.OK, "查询成功", newsList,null);
	}

	/**
	 * 根据ID和token中的用户id查询Activity
	 * @param id
	 * @param userID
	 * @return
	 */
	public Result findByIdandUserID(String id, int userID) {
		try {
			News news = newsDao.findByIdAndUserID(id, userID);
			return new Result(true, StatusCode.OK, "查询成功", news,null);
		} catch (Exception e) {
			throw new RuntimeException("查询异常,请重试");
		}
	}

	/**
	 * 根据id和登录用户的id删除对应的资讯实现
	 * @param id
	 * @param userID
	 * @return
	 */
	@Transactional
	public Result deleteByIdandUserID(String id, int userID) {
		try {
			newsDao.deleteByIdAndUserID(id,userID);
			return new Result(true, StatusCode.OK, "删除成功");
		} catch(Exception e) {
			throw new RuntimeException("删除异常,请重试");
		}
	}
}
