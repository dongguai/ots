package com.dtcloud.ots.manage.service.gen;


import com.dtcloud.ots.manage.dao.gen.HeadlineDao;
import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.manage.model.gen.Banner;
import com.dtcloud.ots.manage.model.gen.Headline;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class HeadlineService {

	@Autowired
	private HeadlineDao headlineDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Headline> findAll() {
		return headlineDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Headline> findSearch(Map whereMap, int page, int size) {
		Specification<Headline> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return headlineDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Headline> findSearch(Map whereMap) {
		Specification<Headline> specification = createSpecification(whereMap);
		return headlineDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Headline findById(String id) {
		return headlineDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param headline
	 */
	public void add(Headline headline) {
		headline.setId( idWorker.nextId()+"" );
		//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
		headline.setState("0");
		//创建日期由运营商自主选择.只填入更新时间
		headline.setUpdated(new Date());
		headlineDao.save(headline);
	}

	/**
	 * 修改头条信息
	 * @param headline
	 */
	public void update(Headline headline) {
		headline.setUpdated(new Date());
		headline.setState("0");
		headlineDao.save(headline);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		headlineDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Headline> createSpecification(Map searchMap) {

		return new Specification<Headline>() {

			@Override
			public Predicate toPredicate(Root<Headline> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 头条id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 头条标题
                if (searchMap.get("title")!=null && !"".equals(searchMap.get("title"))) {
                	predicateList.add(cb.like(root.get("title").as(String.class), "%"+(String)searchMap.get("title")+"%"));
                }
                // 标签(0热门,1最新)
                if (searchMap.get("label")!=null && !"".equals(searchMap.get("label"))) {
                	predicateList.add(cb.like(root.get("label").as(String.class), "%"+(String)searchMap.get("label")+"%"));
                }
                // 头条内容
                if (searchMap.get("content")!=null && !"".equals(searchMap.get("content"))) {
                	predicateList.add(cb.like(root.get("content").as(String.class), "%"+(String)searchMap.get("content")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

	/**
	 * 更新状态,用于审核是否发布
	 * @param map
	 * @return
	 */
	public Result updateStatus(Map<String, String> map) {
		String id = map.get("id");
		String state = map.get("state");
		String userID = map.get("userID");
		try {
			Headline headline = headlineDao.findById(id).get();
			if (headline.getUserID()!=Integer.parseInt(userID)){
			    throw new RuntimeException("审批异常,请重新登录后操作");
			}
			headline.setState(state);
			headline.setUpdated(new Date());
			headlineDao.save(headline);
			return new Result(true, StatusCode.OK, "操作成功");
		} catch (Exception e) {
			throw new RuntimeException("审批异常,请重新登录后操作");
		}
	}

	/**
	 * 根据ID和token中的用户id查询Activity
	 * @param id
	 * @param userID
	 * @return
	 */
	public Result findByIdandUserID(String id, int userID) {
		try {
			Headline headline = headlineDao.findByIdAndUserID(id, userID);
			return new Result(true, StatusCode.OK, "查询成功",headline,null);
		} catch(Exception e) {
		    throw new RuntimeException("查询异常,请重试");
		}
	}

	/**
	 * 查找当前登录用户发布的头条信息,并按照创建时间倒叙排列
	 * @param userID
	 * @return
	 */
	public Result findByUserID(int userID) {
		List<Headline> headlineList = headlineDao.findByUserIDOrderByCreated(userID);
		if (headlineList==null){
			return new Result(true, StatusCode.OK, "无头条信息");
		}
		return new Result(true, StatusCode.OK, "查询成功", headlineList,null);

	}

	/**
	 * 根据id和登录用户的id删除头条实现
	 * @param id
	 * @param userID
	 * @return
	 */
	@Transactional
	public Result deleteByIdandUserID(String id, int userID) {
		try {
			headlineDao.deleteByIdAndUserID(id,userID);
			return new Result(true, StatusCode.OK, "删除成功");
		} catch(Exception e) {
			throw new RuntimeException("删除异常,请重试");
		}
	}
}
