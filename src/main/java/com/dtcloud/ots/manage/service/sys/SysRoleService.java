package com.dtcloud.ots.manage.service.sys;

import com.dtcloud.ots.manage.dao.sys.SysRoleDao;
import com.dtcloud.ots.manage.model.sys.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class SysRoleService {

	@Autowired
	private SysRoleDao sysRoleDao;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<SysRole> findAll() {
		return sysRoleDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<SysRole> findSearch(Map whereMap, int page, int size) {
		Specification<SysRole> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return sysRoleDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<SysRole> findSearch(Map whereMap) {
		Specification<SysRole> specification = createSpecification(whereMap);
		return sysRoleDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public SysRole findById(Integer id) {
		return sysRoleDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param role
	 */
	public void add(SysRole role) {
		sysRoleDao.save(role);
	}

	/**
	 * 修改
	 * @param role
	 */
	public void update(SysRole role) {
		sysRoleDao.save(role);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(Integer id) {
		sysRoleDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<SysRole> createSpecification(Map searchMap) {

		return new Specification<SysRole>() {

			@Override
			public Predicate toPredicate(Root<SysRole> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 角色名
                if (searchMap.get("rolename")!=null && !"".equals(searchMap.get("rolename"))) {
                	predicateList.add(cb.like(root.get("rolename").as(String.class), "%"+(String)searchMap.get("rolename")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
