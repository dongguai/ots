package com.dtcloud.ots.manage.service.gen;


import com.dtcloud.ots.manage.dao.gen.ActivityDao;
import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ActivityService {

	@Autowired
	private ActivityDao activityDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表 默认按照创建时间排序
	 * @return
	 */
	public List<Activity> findAll() {
		return activityDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Activity> findSearch(Map whereMap, int page, int size) {
		Specification<Activity> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return activityDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Activity> findSearch(Map whereMap) {
		Specification<Activity> specification = createSpecification(whereMap);
		return activityDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Activity findById(String id) {
		return activityDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param activity
	 */
	public void add(Activity activity) {
		activity.setId( idWorker.nextId()+"" );
		//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
		activity.setState("0");
		//创建日期由运营商自主选择.只填入更新时间
		activity.setUpdated(new Date());
		activityDao.save(activity);
	}

	/**
	 * 修改活动
	 * @param activity
	 */
	public void update(Activity activity) {
        //更新修改时间
		activity.setUpdated(new Date());
		//修改后需要重置为未发布状态
		activity.setState("0");
		activityDao.save(activity);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		activityDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Activity> createSpecification(Map searchMap) {

		return new Specification<Activity>() {

			@Override
			public Predicate toPredicate(Root<Activity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 活动标题
                if (searchMap.get("title")!=null && !"".equals(searchMap.get("title"))) {
                	predicateList.add(cb.like(root.get("title").as(String.class), "%"+(String)searchMap.get("title")+"%"));
                }
                // 活动图片路径
                if (searchMap.get("image")!=null && !"".equals(searchMap.get("image"))) {
                	predicateList.add(cb.like(root.get("image").as(String.class), "%"+(String)searchMap.get("image")+"%"));
                }
                // 活动状态 (1即将开始,2已开始,3已结束,默认1)
                if (searchMap.get("status")!=null && !"".equals(searchMap.get("status"))) {
                	predicateList.add(cb.like(root.get("status").as(String.class), "%"+(String)searchMap.get("status")+"%"));
                }
                // 攻略内容
                if (searchMap.get("strategycontent")!=null && !"".equals(searchMap.get("strategycontent"))) {
                	predicateList.add(cb.like(root.get("strategycontent").as(String.class), "%"+(String)searchMap.get("strategycontent")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

	/**
	 * 更新状态,用于审核是否发布
	 * @param map
	 * @return
	 */
	public Result updateStatus(Map<String, String> map ){
        String id = map.get("id");
        String state = map.get("state");
        String userID = map.get("userID");
        try {
            Activity activity = activityDao.findById(id).get();
            if (activity.getUserID()!=Integer.parseInt(userID)){
                throw new RuntimeException("审批异常,请重新登录后操作");
            }
            activity.setState(state);
            activity.setUpdated(new Date());
            activityDao.save(activity);
            return new Result(true, StatusCode.OK, "操作成功");
        } catch (RuntimeException e) {
            throw new RuntimeException("审批异常,请重新登录后操作");
        }
    }

	/**
	 * 查找当前登录用户发布的活动信息,并按照创建时间倒叙排列
	 * @param userID
	 * @return
	 */
	public Result findByUserID(int userID) {
        List<Activity> activityList = activityDao.findByUserID(userID);
        if (activityList==null){
            return new Result(true, StatusCode.OK, "无活动信息");
        }
        return new Result(true, StatusCode.OK, "查询成功", activityList,null);
	}

    /**
     * 根据ID和token中的用户id查询Activity
     * @param id
     * @param userID
     * @return
     */
    public Result findByIdandUserID(String id, int userID) {
        try {
            Activity activity = activityDao.findByIdAndUserID(id, userID);
            return new Result(true, StatusCode.OK, "查询成功", activity,null);
        } catch (Exception e) {
            throw new RuntimeException("查询异常,请重试");
        }
    }

    /**
     *     根据id和登录用户的id删除对应的活动
     * @param id
     * @param userID
     * @return
     */
    @Transactional
    public Result deleteByIdandUserID(String id, int userID) {
        try {
            activityDao.deleteByIdAndUserID(id,userID);
            return new Result(true, StatusCode.OK, "删除成功");
        } catch(Exception e) {
            throw new RuntimeException("删除异常,请重试");
        }
    }
}
