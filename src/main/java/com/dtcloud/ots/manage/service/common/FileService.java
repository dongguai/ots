package com.dtcloud.ots.manage.service.common;

import com.alibaba.fastjson.JSONObject;
import com.dtcloud.ots.utils.FileUtils;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: FileService
 * 功 能:  文件和图片上传逻辑实现
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-22 0:14:07
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
@Service
public class FileService {

    @Value("${web.uploadPath}")
    private String uploadPath;

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    /**
     * 上传文件逻辑
     * @param multipartFile
     * @return
     */
    public Result uploadFile(MultipartFile multipartFile) {
        String req;
        req = FileUtils.singleFileUpload(multipartFile,uploadPath+"api/uploadFile");
        if (req.equals("fail")) {
            logger.error("文件上传失败，文件的名称为："+multipartFile.getOriginalFilename());
            return new Result(false, StatusCode.ERROR, "文件上传失败",req);
        }else {
            JSONObject resultJson = JSONObject.parseObject(req);
            if (resultJson.getString("code").equals("0")) {
                return new Result(true, StatusCode.OK, "上传文件成功",uploadPath+resultJson.getString("data"),null);
            }else {
                logger.error("文件上传失败，文件的名称为："+multipartFile.getOriginalFilename());
                return new Result(false, StatusCode.ERROR, "文件上传失败",req);
            }
        }
    }

    /**
     * 上传图片实现逻辑
     * @param multipartFile
     * @return
     */
    public Result uploadImage(MultipartFile multipartFile) {
        String req;
        req = FileUtils.singleFileUpload(multipartFile,uploadPath+"api/uploadImage");
        if (req.equals("fail")) {
            logger.error("图片上传失败，图片的名称为："+multipartFile.getOriginalFilename());
            return new Result(false, StatusCode.ERROR, "图片上传失败",req);
        }else {
            JSONObject resultJson = JSONObject.parseObject(req);
            if (resultJson.getString("code").equals("0")) {
                return new Result(true, StatusCode.OK, "图片上传成功",uploadPath+resultJson.getString("data"),null);
            }else {
                logger.error("图片上传失败，图片的名称为："+multipartFile.getOriginalFilename());
                return new Result(false, StatusCode.ERROR, "图片上传失败",req);
            }
        }
    }
}
