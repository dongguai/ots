package com.dtcloud.ots.manage.service.sys;


import com.dtcloud.ots.manage.dao.sys.SysResourceDao;
import com.dtcloud.ots.manage.model.sys.SysResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class SysResourceService {

	@Autowired
	private SysResourceDao sysResourceDao;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<SysResource> findAll() {
		return sysResourceDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<SysResource> findSearch(Map whereMap, int page, int size) {
		Specification<SysResource> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return sysResourceDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<SysResource> findSearch(Map whereMap) {
		Specification<SysResource> specification = createSpecification(whereMap);
		return sysResourceDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public SysResource findById(Integer id) {
		return sysResourceDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param resource
	 */
	public void add(SysResource resource) {
		sysResourceDao.save(resource);
	}

	/**
	 * 修改
	 * @param resource
	 */
	public void update(SysResource resource) {
		sysResourceDao.save(resource);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(Integer id) {
		sysResourceDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<SysResource> createSpecification(Map searchMap) {

		return new Specification<SysResource>() {

			@Override
			public Predicate toPredicate(Root<SysResource> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 名称
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                	predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }
                // 类型 0目录 1菜单 2按钮 3URL
                if (searchMap.get("type")!=null && !"".equals(searchMap.get("type"))) {
                	predicateList.add(cb.like(root.get("type").as(String.class), "%"+(String)searchMap.get("type")+"%"));
                }
                // 请求方式 GET POST PUT DELETE
                if (searchMap.get("method")!=null && !"".equals(searchMap.get("method"))) {
                	predicateList.add(cb.like(root.get("method").as(String.class), "%"+(String)searchMap.get("method")+"%"));
                }
                // 预留按钮状态，1查看 2添加 3编辑 4发布 5撤回 6删除 7排序
                if (searchMap.get("button_type")!=null && !"".equals(searchMap.get("button_type"))) {
                	predicateList.add(cb.like(root.get("button_type").as(String.class), "%"+(String)searchMap.get("button_type")+"%"));
                }
                // 数据权限 正则表达式
                if (searchMap.get("data_permission")!=null && !"".equals(searchMap.get("data_permission"))) {
                	predicateList.add(cb.like(root.get("data_permission").as(String.class), "%"+(String)searchMap.get("data_permission")+"%"));
                }
                // 请求地址
                if (searchMap.get("url")!=null && !"".equals(searchMap.get("url"))) {
                	predicateList.add(cb.like(root.get("url").as(String.class), "%"+(String)searchMap.get("url")+"%"));
                }
                // 备注
                if (searchMap.get("remarks")!=null && !"".equals(searchMap.get("remarks"))) {
                	predicateList.add(cb.like(root.get("remarks").as(String.class), "%"+(String)searchMap.get("remarks")+"%"));
                }
                // 是否有叶子/子菜单 1 true 0 false
                if (searchMap.get("leafed")!=null && !"".equals(searchMap.get("leafed"))) {
                	predicateList.add(cb.like(root.get("leafed").as(String.class), "%"+(String)searchMap.get("leafed")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
