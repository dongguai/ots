package com.dtcloud.ots.manage.service.gen;


import com.dtcloud.ots.manage.dao.gen.FeedbackDao;
import com.dtcloud.ots.manage.model.gen.Feedback;
import com.dtcloud.ots.utils.IdWorker;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class FeedbackService {

	@Autowired
	private FeedbackDao feedbackDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表,默认分页查询全部结果回显
	 * @return
	 */
	public Page<Feedback> findAll(Integer page,Integer size) {
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return feedbackDao.findAll(pageRequest);
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Feedback> findSearch(Map whereMap, int page, int size) {
		Specification<Feedback> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return feedbackDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Feedback> findSearch(Map whereMap) {
		Specification<Feedback> specification = createSpecification(whereMap);
		return feedbackDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Feedback findById(String id) {
		return feedbackDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param feedback
	 */
	public void add(Feedback feedback) {
		feedback.setId( idWorker.nextId()+"" );
		feedbackDao.save(feedback);
	}

	/**
	 * 修改
	 * @param feedback
	 */
	public void update(Feedback feedback) {
		feedback.setUpdated(new Date());
		feedbackDao.save(feedback);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		feedbackDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Feedback> createSpecification(Map searchMap) {

		return new Specification<Feedback>() {

			@Override
			public Predicate toPredicate(Root<Feedback> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 反馈id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 填写的反馈的用户名
                if (searchMap.get("username")!=null && !"".equals(searchMap.get("username"))) {
                	predicateList.add(cb.like(root.get("username").as(String.class), "%"+(String)searchMap.get("username")+"%"));
                }
                // 用户反馈内容
                if (searchMap.get("content")!=null && !"".equals(searchMap.get("content"))) {
                	predicateList.add(cb.like(root.get("content").as(String.class), "%"+(String)searchMap.get("content")+"%"));
                }
                // 反馈处理状态(0未处理,1处理中,2已处理,默认0)
                if (searchMap.get("status")!=null && !"".equals(searchMap.get("status"))) {
                	predicateList.add(cb.like(root.get("status").as(String.class), "%"+(String)searchMap.get("status")+"%"));
                }
                // 管理员回复意见
                if (searchMap.get("fbcontent")!=null && !"".equals(searchMap.get("fbcontent"))) {
                	predicateList.add(cb.like(root.get("fbcontent").as(String.class), "%"+(String)searchMap.get("fbcontent")+"%"));
                }
                // 发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0
                if (searchMap.get("state")!=null && !"".equals(searchMap.get("state"))) {
                	predicateList.add(cb.like(root.get("state").as(String.class), "%"+(String)searchMap.get("state")+"%"));
                }
				// 根据创建时间来搜索
				if (searchMap.get("startTime")!=null && !"".equals(searchMap.get("startTime"))) {
					predicateList.add(cb.greaterThanOrEqualTo(root.get("created").as(String.class), (String) searchMap.get("startTime")));
				}
				//根据结束时间来搜索
				if (searchMap.get("endTime")!=null && !"".equals(searchMap.get("endTime"))) {
					predicateList.add(cb.lessThanOrEqualTo(root.get("created").as(String.class), (String) searchMap.get("endTime")));
				}
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

	/**
	 * 管理员回复实现
	 * @param map
	 * @return
	 */
	public Result reply(Map<String, String> map) {
		try {
			String id = map.get("id");
			Feedback feedback = feedbackDao.findById(id).get();
			feedback.setSysuserID(Integer.parseInt(map.get("userID")));
			feedback.setFbcontent(map.get("fbcontent"));
			feedbackDao.save(feedback);
			return new Result(true, StatusCode.OK, "回复成功");
		} catch (Exception e) {
			throw new RuntimeException("回复异常,请稍后再试");
		}
	}
}
