package com.dtcloud.ots.manage.vo;


import lombok.Data;

import java.util.List;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: LoginSuccessVO
 * 功 能:
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-18 14:47:56
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
@Data
public class LoginSuccessVO {

    /**
     * 用户编号
     */
    private String userId;

    /**
     * 用户登录名称
     */
    private String loginName;

    /**
     * 角色信息,暂时一个用户只对应一个角色,为后期预留
     */
    private List<String> roles;

}
