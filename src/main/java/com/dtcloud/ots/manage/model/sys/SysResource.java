package com.dtcloud.ots.manage.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="sys_resource")
public class SysResource implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;//ID
	private Integer pid;//父级id，父级为0
	private String name;//名称
	private String type;//类型 0目录 1菜单 2按钮 3URL
	private String method;//请求方式 GET POST PUT DELETE
	private String button_type;//预留按钮状态，1查看 2添加 3编辑 4发布 5撤回 6删除 7排序
	private String data_permission;//数据权限 正则表达式
	private String url;//请求地址
	private String remarks;//备注
	private String leafed;//是否有叶子/子菜单 1 true 0 false
	private Integer creator;//创建人
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date created;//创建时间
	private Integer updater;//修改人
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改时间

	
	public Integer getId() {		
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPid() {		
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {		
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getMethod() {		
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}

	public String getButton_type() {		
		return button_type;
	}
	public void setButton_type(String button_type) {
		this.button_type = button_type;
	}

	public String getData_permission() {		
		return data_permission;
	}
	public void setData_permission(String data_permission) {
		this.data_permission = data_permission;
	}

	public String getUrl() {		
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getRemarks() {		
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getLeafed() {		
		return leafed;
	}
	public void setLeafed(String leafed) {
		this.leafed = leafed;
	}

	public Integer getCreator() {		
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public Integer getUpdater() {		
		return updater;
	}
	public void setUpdater(Integer updater) {
		this.updater = updater;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}


	
}
