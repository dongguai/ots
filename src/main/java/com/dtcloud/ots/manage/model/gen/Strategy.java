package com.dtcloud.ots.manage.model.gen;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="strategy")
public class Strategy implements Serializable{

	@Id
	private String id;//攻略id
	private String title;//攻略标题
	private String image;//攻略缩略图链接
	private String istop;//是否顶置(1是,0不是,默认0)
	private String author;//攻略作者姓名
	private Integer userID;//发布者id
	private String strategycontent;//攻略内容
	@JsonFormat(pattern = "MM-dd HH:mm", timezone = "GMT+8")
	private java.util.Date created;//创建日期
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改日期
	private String state;//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {		
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public String getIstop() {		
		return istop;
	}
	public void setIstop(String istop) {
		this.istop = istop;
	}

	public String getAuthor() {		
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getUserID() {		
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getStrategycontent() {		
		return strategycontent;
	}
	public void setStrategycontent(String strategycontent) {
		this.strategycontent = strategycontent;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	
}
