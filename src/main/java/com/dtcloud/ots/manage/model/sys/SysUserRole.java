package com.dtcloud.ots.manage.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="sys_user_role")
public class SysUserRole implements Serializable{

	private static final long serialVersionUID = 2928615270581468009L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;//用户角色表id

	private Integer user_id;//用户表id
	private Integer role_id;//角色id
	private Integer creator;//创建信息的用户id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date created;//创建时间
	private Integer updater;//修改信息的用户id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改时间

	
	public Integer getId() {		
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {		
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getRole_id() {		
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public Integer getCreator() {		
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public Integer getUpdater() {		
		return updater;
	}
	public void setUpdater(Integer updater) {
		this.updater = updater;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}


	
}
