package com.dtcloud.ots.manage.model.gen;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="normal_user")
public class User implements Serializable{

	@Id
	private String id;//普通用户id
	private String mobile;//用户名
	private String password;//密码，加密存储
	private String nick_name;//昵称
	private String icon; //用户头像路径地址
	private String sex;//1男 0女
	private String register_source;//注册渠道  1PC,2H5,3Android,4ios
	private String register_area;//注册地区
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date register_time;//注册时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date modifytime;//更新时间
	private String state;//用户状态 (-1已删除,0禁用,1正常)


	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNick_name() {
		return nick_name;
	}

	public void setNick_name(String nick_name) {
		this.nick_name = nick_name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getRegister_source() {
		return register_source;
	}

	public void setRegister_source(String register_source) {
		this.register_source = register_source;
	}

	public String getRegister_area() {
		return register_area;
	}

	public void setRegister_area(String register_area) {
		this.register_area = register_area;
	}

	public Date getRegister_time() {
		return register_time;
	}

	public void setRegister_time(Date register_time) {
		this.register_time = register_time;
	}

	public Date getModifytime() {
		return modifytime;
	}

	public void setModifytime(Date modifytime) {
		this.modifytime = modifytime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
