package com.dtcloud.ots.manage.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="sys_user")
public class SysUser implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;//系统用户id

	private String loginname;//系统用户名,5-16位数字、字母或其组合的账号
	private String loginpassword;//登录密码
	private String realname;//真实名称
	private String nick_name;//用户昵称
	private String icon; //用户头像路径地址
	private Integer creator;//创建人
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date created;//创建时间
	private Integer updater;//更新人id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//更新时间
	private Integer role_id;//用户分配的角色

	public String getNick_name() {
		return nick_name;
	}

	public void setNick_name(String nick_name) {
		this.nick_name = nick_name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginname() {		
		return loginname;
	}
	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getLoginpassword() {		
		return loginpassword;
	}
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}

	public String getRealname() {		
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}

	public Integer getCreator() {		
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public Integer getUpdater() {		
		return updater;
	}
	public void setUpdater(Integer updater) {
		this.updater = updater;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}

	public Integer getRole_id() {		
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}


	
}
