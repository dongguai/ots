package com.dtcloud.ots.manage.model.gen;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="banner")
public class Banner implements Serializable{

	@Id
	private String id;//导航id
	private String title;//导航标题
	private String image;//导航图片路径
	private Integer linktype;//链接类型，1咨询, 2活动，3攻略
	private String link;//首页banner链接类型，其中1外链，2头条，3攻略，4咨询，5活动
	private Integer sort;//导航排序序号（越小越靠前）
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date created;//导航创建时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//导航最后一次修改时间
	private Integer userID;//发布者id，外键
	private String state;//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {		
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public Integer getLinktype() {		
		return linktype;
	}
	public void setLinktype(Integer linktype) {
		this.linktype = linktype;
	}

	public String getLink() {		
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	public Integer getSort() {		
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}

	public Integer getUserID() {		
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	
}
