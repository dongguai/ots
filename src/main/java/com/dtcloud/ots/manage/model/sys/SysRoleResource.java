package com.dtcloud.ots.manage.model.sys;

import javax.persistence.*;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="sys_role_resource")
public class SysRoleResource implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;//角色资源表id


	
	private Integer role_id;//角色id
	private String type;//资源类型 0目录 1菜单 2按钮 3其他
	private Integer resource_id;//资源id
	private java.util.Date created;//创建时间

	
	public Integer getId() {		
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRole_id() {		
		return role_id;
	}
	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public String getType() {		
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Integer getResource_id() {		
		return resource_id;
	}
	public void setResource_id(Integer resource_id) {
		this.resource_id = resource_id;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}


	
}
