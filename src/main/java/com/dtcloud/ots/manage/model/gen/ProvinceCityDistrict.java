package com.dtcloud.ots.manage.model.gen;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="province_city_district")
public class ProvinceCityDistrict implements Serializable{

	@Id
	private String pcd_id;//地区id


	
	private String pcd_name;//地区名称
	private String parent_id;//地区父id
	private String area_code;//地区号
	private String pcd_level;//地区层级
	private String type;//地区类型
	private String pin_yin;//地区汉语拼音

	
	public String getPcd_id() {		
		return pcd_id;
	}
	public void setPcd_id(String pcd_id) {
		this.pcd_id = pcd_id;
	}

	public String getPcd_name() {		
		return pcd_name;
	}
	public void setPcd_name(String pcd_name) {
		this.pcd_name = pcd_name;
	}

	public String getParent_id() {		
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public String getArea_code() {		
		return area_code;
	}
	public void setArea_code(String area_code) {
		this.area_code = area_code;
	}

	public String getPcd_level() {		
		return pcd_level;
	}
	public void setPcd_level(String pcd_level) {
		this.pcd_level = pcd_level;
	}

	public String getType() {		
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getPin_yin() {		
		return pin_yin;
	}
	public void setPin_yin(String pin_yin) {
		this.pin_yin = pin_yin;
	}


	
}
