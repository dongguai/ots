package com.dtcloud.ots.manage.model.gen;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="headline")
public class Headline implements Serializable{

	@Id
	private String id;//头条id
	private String title;//头条标题
	private String label;//标签(0热门,1最新)
	private String content;//头条内容
	private Integer userID;//创建者id
	@JsonFormat(pattern = "MM-dd HH:mm", timezone = "GMT+8")
	private java.util.Date created;//创建时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改时间
	private String state;//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getLabel() {		
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getContent() {		
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUserID() {		
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	
}
