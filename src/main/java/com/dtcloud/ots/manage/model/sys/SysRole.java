package com.dtcloud.ots.manage.model.sys;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="sys_role")
public class SysRole implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;//角色id


	
	private String rolename;//角色名
	private Integer creator;//创建角色的用户id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date created;//创建时间
	private Integer updater;//修改角色的用户id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改时间

	
	public Integer getId() {		
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getRolename() {		
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Integer getCreator() {		
		return creator;
	}
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public Integer getUpdater() {		
		return updater;
	}
	public void setUpdater(Integer updater) {
		this.updater = updater;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}


	
}
