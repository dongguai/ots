package com.dtcloud.ots.manage.model.gen;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="activity")
public class Activity implements Serializable{

	@Id
	private String id;//id
	private String title;//活动标题
	private String image;//活动图片路径
	private String status;//活动状态 (0即将开始,1已开始,2已结束,默认0)
	private Integer peoplenum;//已参与人数(人数建议50人内)
	private String strategycontent;//攻略内容
	private Integer sort;//活动管理排序序号（越小越靠前）
	private Integer userID;//创建者id
	@JsonFormat(pattern = "MM-dd HH:mm", timezone = "GMT+8")
	private java.util.Date created;//创建日期
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date updated;//修改日期
	private String state;//发布状态：0待发布，1发布成功,  2发布撤销，3删除；默认0

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {		
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public String getStatus() {		
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getPeoplenum() {		
		return peoplenum;
	}
	public void setPeoplenum(Integer peoplenum) {
		this.peoplenum = peoplenum;
	}

	public String getStrategycontent() {		
		return strategycontent;
	}
	public void setStrategycontent(String strategycontent) {
		this.strategycontent = strategycontent;
	}

	public Integer getSort() {		
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getUserID() {		
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}

	public String getState() {		
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}


	
}
