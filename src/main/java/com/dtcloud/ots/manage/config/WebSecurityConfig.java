package com.dtcloud.ots.manage.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 功能：
 *     spring security配置类
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-14 21:01:34
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http  //链式调用
                .authorizeRequests()
                .antMatchers("/**").permitAll()  //匹配这个格式,许可所有请求
                .anyRequest().authenticated()
                .and().csrf().disable();
    }
}
