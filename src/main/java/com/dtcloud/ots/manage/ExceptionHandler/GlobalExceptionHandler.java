package com.dtcloud.ots.manage.ExceptionHandler;


import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletRequest;

/**
 * 功能：
 *      统一异常处理类
 * @author：xupeiqing(xupq@hncdcenter.com)
 * @create：2019-04-14 19:12:49
 * @version：2019 Version 1.0
 * @company：河南云数聚 Created with IntelliJ IDEA
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger=LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result error(HttpServletRequest req, Exception e){
        String url = req.getRequestURL().toString();
        logger.error(url);
        return new Result(false, StatusCode.ERROR,e.getMessage(),url);
    }


}
