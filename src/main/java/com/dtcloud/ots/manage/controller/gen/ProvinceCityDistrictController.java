package com.dtcloud.ots.manage.controller.gen;


import com.dtcloud.ots.manage.model.gen.ProvinceCityDistrict;
import com.dtcloud.ots.manage.service.gen.ProvinceCityDistrictService;
import com.dtcloud.ots.utils.entity.PageResult;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/provinceCityDistrict")
public class ProvinceCityDistrictController {

	@Autowired
	private ProvinceCityDistrictService provinceCityDistrictService;
	
	
	/**
	 * 查询全部数据
	 * @return
	 */
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功", provinceCityDistrictService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功", provinceCityDistrictService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<ProvinceCityDistrict> pageList = provinceCityDistrictService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<ProvinceCityDistrict>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功", provinceCityDistrictService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param provinceCityDistrict
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody ProvinceCityDistrict provinceCityDistrict  ){
		provinceCityDistrictService.add(provinceCityDistrict);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param provinceCityDistrict
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody ProvinceCityDistrict provinceCityDistrict, @PathVariable String id ){
		provinceCityDistrict.setPcd_id(id);
		provinceCityDistrictService.update(provinceCityDistrict);
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		provinceCityDistrictService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}
	
}
