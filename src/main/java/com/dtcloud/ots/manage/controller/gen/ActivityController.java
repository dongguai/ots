package com.dtcloud.ots.manage.controller.gen;

import com.dtcloud.ots.manage.model.gen.Activity;
import com.dtcloud.ots.manage.service.gen.ActivityService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 前端控制层接口控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/activity")
public class ActivityController {

	@Autowired
	private ActivityService activityService;
	
	

	/**
	 * 增加
	 * @param activity
	 */
//	@PreAuthorize("")
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Activity activity , HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        activity.setUserID(Integer.parseInt(userID));
        activityService.add(activity);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param activity
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Activity activity, @PathVariable String id,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        activity.setUserID(Integer.parseInt(userID));
        activity.setId(id);
        activityService.update(activity);
		return new Result(true, StatusCode.OK, "修改成功");
	}
	
	/**
	 * 根据id和登录用户的id删除活动
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        return activityService.deleteByIdandUserID(id,Integer.parseInt(userID));
	}

	/**
	 * 根据id更新activity状态,需要着token防止审批其他人的,用于操作时候的审核操作.
	 * @return
	 */
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	public Result updateStatus(@RequestBody Map<String, String> map,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        map.put("userID", userID);
        return activityService.updateStatus(map);
	}

}
