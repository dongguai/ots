package com.dtcloud.ots.manage.controller.gen;

import com.dtcloud.ots.manage.model.gen.Headline;
import com.dtcloud.ots.manage.service.gen.HeadlineService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/headline")
public class HeadlineController {

	@Autowired
	private HeadlineService headlineService;
	
	/**
	 * 增加
	 * @param headline
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Headline headline , HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
		headline.setUserID(Integer.parseInt(userID));
		headlineService.add(headline);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param headline
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Headline headline, @PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        headline.setUserID(Integer.parseInt(userID));
		headline.setId(id);
		headlineService.update(headline);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 根据id和登录用户的id删除头条
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        return headlineService.deleteByIdandUserID(id,Integer.parseInt(userID));
	}

	/**
	 * 根据id更新headline状态,用于操作时候的审核操作.
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	public Result updateStatus(@RequestBody Map<String, String> map, HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        map.put("userID", userID);
		return headlineService.updateStatus(map);
	}
}
