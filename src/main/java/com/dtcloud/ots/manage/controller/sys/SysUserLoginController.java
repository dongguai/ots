package com.dtcloud.ots.manage.controller.sys;

import com.dtcloud.ots.manage.model.sys.SysResource;
import com.dtcloud.ots.manage.model.sys.SysRole;
import com.dtcloud.ots.manage.model.sys.SysUser;
import com.dtcloud.ots.manage.service.sys.*;
import com.dtcloud.ots.utils.JwtUtil;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: SysUserLoginController
 * 功 能: 系统用户登录控制器
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-18 10:38:03
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */

@RestController
@RequestMapping("/auth")
public class SysUserLoginController {

    @Autowired
    private SysUserService sysUserService;


    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysResourceService sysResourceService;

    @Autowired
    private SysRoleResourceService sysRoleResourceService;

    @Autowired
    private JwtUtil jwtUtil;


    /**
     * 用户登录
     *
     * @param map
     */
    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public Result login(@RequestBody Map<String, String> map) {
        String loginName = map.get("loginName");
        String password = map.get("password");
        if (StringUtils.isEmpty(loginName) || StringUtils.isEmpty(password)) {
            return new Result(false, StatusCode.ACCESSERROR, "用户名或密码不能为空");
        }
        //获取的登录的系统用户
        SysUser sysUser = sysUserService.findByLoginName(loginName, password);
        if (sysUser != null) {
            //查询该用户对应的角色信息,根据原形暂定一个用户只有一个角色
            SysRole sysRole = sysRoleService.findById(sysUser.getRole_id());
            //查询该用户所对应的资源集合
            List<SysResource> resourceList=null;
            //登录成功后生成token
            String token = jwtUtil.createJWT(sysUser.getId() + "", sysUser.getLoginname(), sysRole.getRolename(), resourceList);
            Map resultMap=new HashMap();
            resultMap.put("token", token);
            resultMap.put("loginName", loginName);
            resultMap.put("id", sysUser.getId());
            return new Result(true, StatusCode.OK, "登录成功", resultMap);
        }
        return new Result(false, StatusCode.LOGINERROR, "登录失败");

    }
}
