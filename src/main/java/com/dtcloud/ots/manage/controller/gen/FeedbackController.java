package com.dtcloud.ots.manage.controller.gen;


import com.dtcloud.ots.manage.model.gen.Feedback;
import com.dtcloud.ots.manage.service.gen.FeedbackService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/feedback")
public class FeedbackController {

	@Autowired
	private FeedbackService feedbackService;
	
	/**
	 * 增加
	 * @param feedback
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Feedback feedback , HttpServletRequest req){
	    //获取当前登录的系统用户的id,并绑定到对象中
        String userID = (String) req.getAttribute("userID");
        feedback.setUserID(Integer.parseInt(userID));
        feedbackService.add(feedback);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param feedback
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Feedback feedback, @PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        feedback.setUserID(Integer.parseInt(userID));
        feedback.setId(id);
        feedbackService.update(feedback);
		return new Result(true,StatusCode.OK,"修改成功");
	}

    /**
     * 管理员回复反馈
     */
    @RequestMapping(value="/reply",method= RequestMethod.PUT)
    public Result reply(@RequestBody Map<String,String> map, HttpServletRequest req){
        String sysuserID = (String) req.getAttribute("userID");
        map.put("userID", sysuserID);
        return feedbackService.reply(map);
    }
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		feedbackService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}
	
}
