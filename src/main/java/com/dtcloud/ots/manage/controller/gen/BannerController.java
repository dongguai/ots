package com.dtcloud.ots.manage.controller.gen;

import com.dtcloud.ots.manage.model.gen.Banner;
import com.dtcloud.ots.manage.service.gen.BannerService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import com.sun.org.apache.regexp.internal.RE;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/banner")
public class BannerController {

	@Autowired
	private BannerService bannerService;

	
	/**
	 * 增加
	 * @param banner
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Banner banner  , HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        banner.setUserID(Integer.parseInt(userID));
		bannerService.add(banner);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param banner
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Banner banner, @PathVariable String id,HttpServletRequest req ){
        String userID = (String) req.getAttribute("userID");
        banner.setUserID(Integer.parseInt(userID));
		banner.setId(id);
		bannerService.update(banner);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 根据id和登录用户的id删除 banner
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        return bannerService.deleteByIdandUserID(id,Integer.parseInt(userID));
	}

	/**
	 * 根据id更新banner状态,用于操作时候的审核操作.
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	public Result updateStatus(@RequestBody Map<String, String> map, HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        map.put("userID", userID);
        return bannerService.updateStatus(map);
	}

	
}
