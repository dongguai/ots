package com.dtcloud.ots.manage.controller.gen;


import com.dtcloud.ots.manage.model.gen.News;
import com.dtcloud.ots.manage.service.gen.NewsService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private NewsService newsService;
	
	/**
	 * 增加
	 * @param news
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody News news , HttpServletRequest req){
		String id = (String) req.getAttribute("userID");
		news.setUserID(Integer.parseInt(id));
		newsService.add(news);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param news
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody News news, @PathVariable String id ,HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
		news.setId(id);
		news.setUserID(Integer.parseInt(userID));
		newsService.update(news);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 根据id和登录用户的id删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id,HttpServletRequest req ){
		String userID = (String) req.getAttribute("userID");
		return newsService.deleteByIdandUserID(id,Integer.parseInt(userID));
	}

	/**
	 * 根据id更新news状态,用于操作时候的审核操作.
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	public Result updateStatus(@RequestBody Map<String, String> map, HttpServletRequest req){
        String userID = (String) req.getAttribute("userID");
        map.put("userID", userID);
        return newsService.updateStatus(map);
	}
	
}
