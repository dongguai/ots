package com.dtcloud.ots.manage.controller.gen;


import com.dtcloud.ots.manage.model.gen.Strategy;
import com.dtcloud.ots.manage.service.gen.StrategyService;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/strategy")
public class StrategyController {

	@Autowired
	private StrategyService strategyService;
	
	/**
	 * 增加
	 * @param strategy
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Strategy strategy , HttpServletRequest req){
		String userID = (String) req.getAttribute("userID");
		strategy.setUserID(Integer.parseInt(userID));
		strategyService.add(strategy);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param strategy
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Strategy strategy, @PathVariable String id,HttpServletRequest req ){
		String userID = (String) req.getAttribute("userID");
		strategy.setUserID(Integer.parseInt(userID));
		strategy.setId(id);
		strategyService.update(strategy);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 根据id和登录用户的id删除攻略
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ,HttpServletRequest req){
		String userID = (String) req.getAttribute("userID");
		return strategyService.deleteByIdandUserID(id,Integer.parseInt(userID));
	}


	/**
	 * 根据id更新strategy状态,用于操作时候的审核操作.
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
	public Result updateStatus(@RequestBody Map<String, String> map, HttpServletRequest req){
		String userID = (String) req.getAttribute("userID");
		map.put("userID", userID);
		return strategyService.updateStatus(map);
	}
	
}
