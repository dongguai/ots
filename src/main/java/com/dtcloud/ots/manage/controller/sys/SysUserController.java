package com.dtcloud.ots.manage.controller.sys;


import com.dtcloud.ots.manage.model.sys.SysUser;
import com.dtcloud.ots.manage.service.sys.*;
import com.dtcloud.ots.utils.JwtUtil;
import com.dtcloud.ots.utils.entity.PageResult;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 增加用户
     *
     * @param user
     */
    @RequestMapping(method = RequestMethod.POST,value = "/addUser")
    public Result addSysUser(@RequestBody SysUser user , HttpServletRequest req) {
        //添加创建当前账号的用户账号id
        String id = (String) req.getAttribute("userID");
        user.setCreator(Integer.parseInt(id));
        return sysUserService.add(user);
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", sysUserService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable Integer id) {
        return new Result(true, StatusCode.OK, "查询成功", sysUserService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<SysUser> pageList = sysUserService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<SysUser>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", sysUserService.findSearch(searchMap));
    }

    /**
     * 修改
     *
     * @param user
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody SysUser user, @PathVariable Integer id) {
        user.setId(id);
        sysUserService.update(user);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable Integer id) {
        sysUserService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
