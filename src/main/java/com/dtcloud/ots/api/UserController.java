package com.dtcloud.ots.api;


import com.dtcloud.ots.manage.service.common.UserService;
import com.dtcloud.ots.manage.model.gen.User;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: FileService
 * 功 能:  普通用户管理前端接口
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-20 0:14:07
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 发送短信接口
     * @param mobile
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/sendsms/{mobile}")
    public Result sendsms(@PathVariable(value = "mobile", required = true) String mobile) {
        userService.sendsms(mobile);
        return new Result(true, StatusCode.OK, "发送验证码成功");
    }

    /**
     * 登录验证接口
     * @param map
     */
    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public Result login(@RequestBody Map<String, String> map) {
        return userService.login(map);
    }

    /**
     * 用户注册
     */
    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public Result register(@RequestBody Map<String, String> map) {
        return userService.register(map);
    }

    /**
     * 用户注册时校验验证码
     * @param map
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/register/checkCode")
    public Result registerCheckCode(@RequestBody Map<String, String> map) {
        return userService.registerCheckCode(map);
    }


    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id,HttpServletRequest request) {
        if (checkAuth(request,id)){
            return new Result(true, StatusCode.OK, "查询成功", userService.findById(id));
        }
        return new Result(false, StatusCode.ACCESSERROR, "权限不足");
    }

    /**
     * 修改
     *
     * @param user
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody User user, @PathVariable(value = "id", required = true) String id, HttpServletRequest request) {
        if (checkAuth(request,id)){
            user.setId(id);
            userService.update(user);
            return new Result(true, StatusCode.OK, "修改成功");
        }
        return new Result(false, StatusCode.ACCESSERROR, "权限不足");
    }

    /**
     * 校验当前登录的普通用户和要获取的用户的资源是否匹配
     * @param request
     * @param id
     * @return
     */
    private boolean checkAuth(HttpServletRequest request, String id) {
        Claims claims = (Claims) request.getAttribute("user_claims");
        if (claims != null && id.equals(claims.getId())) {
            return true;
        }
        return false;
    }


}
