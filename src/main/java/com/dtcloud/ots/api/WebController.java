package com.dtcloud.ots.api;

import com.dtcloud.ots.manage.model.gen.*;
import com.dtcloud.ots.manage.model.sys.SysUser;
import com.dtcloud.ots.manage.service.gen.*;
import com.dtcloud.ots.manage.service.sys.SysRoleService;
import com.dtcloud.ots.manage.service.sys.SysUserService;
import com.dtcloud.ots.utils.entity.PageResult;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: WebController
 * 功 能: Web端公共控制类
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-19 14:12:28
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
@RestController
@RequestMapping("/web")
public class WebController {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private BannerService bannerService;
    @Autowired
    private FeedbackService feedbackService;
    @Autowired
    private HeadlineService headlineService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private StrategyService strategyService;
    @Autowired
    private SysRoleService sysRoleService;


    @Autowired
    private SysUserService sysUserService;


    /**
     * 登录验证接口
     *
     * @param map
     */
    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public Result login(@RequestBody Map<String, String> map) {
        return sysUserService.login(map);
    }

    /**
     * 查询全部的角色信息
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findSysRoleAll")
    public Result findSysRoleAll() {
        return new Result(true, StatusCode.OK, "查询成功", sysRoleService.findAll());
    }


    /**
     * 查询当前登录用户自己创建的Activity
     *
     * @return
     */
    @RequestMapping(value = "/findActivityAll", method = RequestMethod.GET)
    public Result findActivityAll(HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return activityService.findByUserID(Integer.parseInt(userID));
    }

    /**
     * 根据ID和token中的用户id查询Activity
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findActivityById/{id}", method = RequestMethod.GET)
    public Result findActivityById(@PathVariable String id, HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return activityService.findByIdandUserID(id, Integer.parseInt(userID));
    }


    /**
     * 分页+多条件查询Activity
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/findActivitySearch/{page}/{size}", method = RequestMethod.POST)
    public Result findActivitySearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Activity> pageList = activityService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Activity>(pageList.getTotalElements(), pageList.getContent()));
    }

    /******************************************************************************************************************/
    /**
     * 查询Banner全部数据
     *
     * @return
     */
    @RequestMapping(value = "/findBannerAll", method = RequestMethod.GET)
    public Result findBannerAll() {
        return new Result(true, StatusCode.OK, "查询成功", bannerService.findAll());
    }

    /**
     * 根据ID和登录的用户的id查询Banner
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findBannerById/{id}", method = RequestMethod.GET)
    public Result findBannerById(@PathVariable String id,HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return  bannerService.findByIdandUserID(id, Integer.parseInt(userID));
    }


    /**
     * 分页+多条件查询Banner
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/findBannerSearch/{page}/{size}", method = RequestMethod.POST)
    public Result findBannerSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Banner> pageList = bannerService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Banner>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询Banner
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/findBannerSearch", method = RequestMethod.POST)
    public Result findBannerSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", bannerService.findSearch(searchMap));
    }
/******************************************************************************************************************/
    /**
     * 查询Feedback全部数据,分页查询回显
     *
     * @return
     */
    @RequestMapping(value = "/findFeedbackAll", method = RequestMethod.GET)
    public Result findFeedbackAll() {
        Integer size=12;
        Integer page=1;
        Page<Feedback> pageList = feedbackService.findAll(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Feedback>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据ID查询Feedback
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findFeedbackById/{id}", method = RequestMethod.GET)
    public Result findFeedbackById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", feedbackService.findById(id));
    }


    /**
     * 分页+多条件查询Feedback信息
     *
     * @param searchMap 查询条件封装
     * @return 分页结果
     */
    @RequestMapping(value = "/findFeedbackSearch/{page}", method = RequestMethod.POST)
    public Result findFeedbackSearch(@RequestBody Map searchMap,@PathVariable Integer page) {
        //设置单页显示12条数据
        Integer size = 12;
        Page<Feedback> pageList = feedbackService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Feedback>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询Feedback
     *
     * @param searchMap
     * @return
    @RequestMapping(value = "/findFeedbackSearch", method = RequestMethod.POST)
    public Result findFeedbackSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", feedbackService.findSearch(searchMap));
    }*/
/******************************************************************************************************************/

    /**
     * 查询当前登录用户的全部数据Headline
     *
     * @return
     */
    @RequestMapping(value = "/findHeadlineAll", method = RequestMethod.GET)
    public Result findHeadlineAll(HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return headlineService.findByUserID(Integer.parseInt(userID));
    }

    /**
     * 根据ID和登录的用户的token查询Headline
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findHeadlineById/{id}", method = RequestMethod.GET)
    public Result findHeadlineById(@PathVariable String id,HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return headlineService.findByIdandUserID(id, Integer.parseInt(userID));
    }


    /**
     * 分页+多条件查询Headline
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/findHeadlineSearch/{page}/{size}", method = RequestMethod.POST)
    public Result findHeadlineSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Headline> pageList = headlineService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Headline>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询Headline
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/findHeadlineSearch", method = RequestMethod.POST)
    public Result findHeadlineSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", headlineService.findSearch(searchMap));
    }

/******************************************************************************************************************/
    /**
     * 查询全部当前登录用户自己创建的News
     *
     * @return
     */
    @RequestMapping(value = "/findNewsAll", method = RequestMethod.GET)
    public Result findNewsAll(HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return newsService.findByUserID(Integer.parseInt(userID));
    }

    /**
     * 根据ID和token中的登陆用户的id查询News
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findNewsById/{id}", method = RequestMethod.GET)
    public Result findNewsById(@PathVariable String id, HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return  newsService.findByIdandUserID(id, Integer.parseInt(userID));
    }


    /**
     * 分页+多条件查询News
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/findNewsSearch/{page}/{size}", method = RequestMethod.POST)
    public Result findNewsSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<News> pageList = newsService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<News>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询News
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/findNewsSearch", method = RequestMethod.POST)
    public Result findNewsSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", newsService.findSearch(searchMap));
    }
/******************************************************************************************************************/
    /**
     * 查询全部数据Strategy
     *
     * @return
     */
    @RequestMapping(value = "/findStrategyAll", method = RequestMethod.GET)
    public Result findStrategyAll() {
        return new Result(true, StatusCode.OK, "查询成功", strategyService.findAll());
    }

    /**
     * 根据ID和token中的登陆用户的id查询Strategy
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/findStrategyById/{id}", method = RequestMethod.GET)
    public Result findStrategyById(@PathVariable String id,HttpServletRequest req) {
        String userID = (String) req.getAttribute("userID");
        return strategyService.findByIdandUserID(id, Integer.parseInt(userID));
    }


    /**
     * 分页+多条件查询Strategy
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/findStrategySearch/{page}/{size}", method = RequestMethod.POST)
    public Result findStrategySearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Strategy> pageList = strategyService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Strategy>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询Strategy
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/findStrategySearch", method = RequestMethod.POST)
    public Result findStrategySearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", strategyService.findSearch(searchMap));
    }


}
