package com.dtcloud.ots.api;

import com.dtcloud.ots.manage.model.gen.*;
import com.dtcloud.ots.manage.service.gen.*;
import com.dtcloud.ots.utils.entity.PageResult;
import com.dtcloud.ots.utils.entity.Result;
import com.dtcloud.ots.utils.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

/**
 * ------------------------------------------------------------------
 * Copyright © 2019 HeNan DtCloud Network Technology Co.,Lt d.
 * ------------------------------------------------------------------
 * 类 名: AppController
 * 功 能:  App端公共控制类接口
 * 创建者: xupeiqing(xupq@hncdcenter.com)
 * 创建时间: 2019-04-19 14:11:59
 * 备 注:
 * ------------------------------------------------------------------
 * 修改历史
 * ------------------------------------------------------------------
 * 时间                      姓名                  备注
 * ------------------------------------------------------------------
 * <p>
 * ------------------------------------------------------------------
 */
@RestController
@RequestMapping("/app")
public class AppController {
    @Autowired
    private ActivityService activityService;
    @Autowired
    private BannerService bannerService;
    @Autowired
    private FeedbackService feedbackService;
    @Autowired
    private HeadlineService headlineService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private StrategyService strategyService;

    /**
     * 查询Activity全部数据
     * @return
     */
    @RequestMapping(value = "/findActivityAll",method= RequestMethod.GET)
    public Result findActivityAll(){
        return new Result(true,StatusCode.OK,"查询成功",activityService.findAll());
    }

    /**
     * 根据ID查询Activity
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findActivityById/{id}",method= RequestMethod.GET)
    public Result findActivityById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",activityService.findById(id));
    }


    /**
     * 分页+多条件查询Activity
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findActivitySearch/{page}/{size}",method=RequestMethod.POST)
    public Result findActivitySearch(@RequestBody Map searchMap , @PathVariable() int page, @PathVariable int size){
        Page<Activity> pageList = activityService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Activity>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询Activity
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findActivitySearch",method = RequestMethod.POST)
    public Result findActivitySearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",activityService.findSearch(searchMap));
    }

 /******************************************************************************************************************/
    /**
     * 查询Banner全部数据
     * @return
     */
    @RequestMapping(value = "/findBannerAll",method= RequestMethod.GET)
    public Result findBannerAll(){
        return new Result(true,StatusCode.OK,"查询成功",bannerService.findAll());
    }

    /**
     * 根据ID查询Banner
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findBannerById/{id}",method= RequestMethod.GET)
    public Result findBannerById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",bannerService.findById(id));
    }


    /**
     * 分页+多条件查询Banner
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findBannerSearch/{page}/{size}",method=RequestMethod.POST)
    public Result findBannerSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Banner> pageList = bannerService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Banner>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询Banner
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findBannerSearch",method = RequestMethod.POST)
    public Result findBannerSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",bannerService.findSearch(searchMap));
    }
/******************************************************************************************************************/
    /**
     * 根据ID查询Feedback
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findFeedbackById/{id}",method= RequestMethod.GET)
    public Result findFeedbackById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",feedbackService.findById(id));
    }


    /**
     * 分页+多条件查询Feedback信息
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findFeedbackSearch/{page}/{size}",method=RequestMethod.POST)
    public Result findFeedbackSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Feedback> pageList = feedbackService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Feedback>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询Feedback
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findFeedbackSearch",method = RequestMethod.POST)
    public Result findFeedbackSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",feedbackService.findSearch(searchMap));
    }
/******************************************************************************************************************/

    /**
     * 查询全部数据Headline
     * @return
     */
    @RequestMapping(value = "/findHeadlineAll",method= RequestMethod.GET)
    public Result findHeadlineAll(){
        return new Result(true,StatusCode.OK,"查询成功",headlineService.findAll());
    }

    /**
     * 根据ID查询Headline
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findHeadlineById/{id}",method= RequestMethod.GET)
    public Result findHeadlineById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",headlineService.findById(id));
    }


    /**
     * 分页+多条件查询Headline
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findHeadlineSearch/{page}/{size}",method=RequestMethod.POST)
    public Result findHeadlineSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Headline> pageList = headlineService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Headline>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询Headline
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findHeadlineSearch",method = RequestMethod.POST)
    public Result findHeadlineSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",headlineService.findSearch(searchMap));
    }

/******************************************************************************************************************/
    /**
     * 查询全部News数据
     * @return
     */
    @RequestMapping(value = "/findNewsAll",method= RequestMethod.GET)
    public Result findNewsAll(){
        return new Result(true,StatusCode.OK,"查询成功",newsService.findAll());
    }

    /**
     * 根据ID查询News
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findNewsById/{id}",method= RequestMethod.GET)
    public Result findNewsById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",newsService.findById(id));
    }


    /**
     * 分页+多条件查询News
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findNewsSearch/{page}/{size}",method=RequestMethod.POST)
    public Result findNewsSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<News> pageList = newsService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<News>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询News
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findNewsSearch",method = RequestMethod.POST)
    public Result findNewsSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",newsService.findSearch(searchMap));
    }
/******************************************************************************************************************/
    /**
     * 查询全部数据Strategy
     * @return
     */
    @RequestMapping(value = "/findStrategyAll",method= RequestMethod.GET)
    public Result findStrategyAll(){
        return new Result(true,StatusCode.OK,"查询成功",strategyService.findAll());
    }

    /**
     * 根据ID查询Strategy
     * @param id ID
     * @return
     */
    @RequestMapping(value="/findStrategyById/{id}",method= RequestMethod.GET)
    public Result findStrategyById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",strategyService.findById(id));
    }


    /**
     * 分页+多条件查询Strategy
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/findStrategySearch/{page}/{size}",method=RequestMethod.POST)
    public Result findStrategySearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Strategy> pageList = strategyService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Strategy>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询Strategy
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findStrategySearch",method = RequestMethod.POST)
    public Result findStrategySearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",strategyService.findSearch(searchMap));
    }


}
