package com.dtcloud.ots;

import com.dtcloud.ots.manage.dao.gen.BannerDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OtsApplicationTests {

    @Autowired
    private BannerDao b;
    @Test
    public void contextLoads() {
        Integer maxSort = b.findMaxSort();
        System.out.println(maxSort);
    }

}
